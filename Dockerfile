# Use the pgAdmin4 image
FROM bitnami/pgadmin4

# Set environment variables for pgadmin
ENV PGADMIN_DEFAULT_EMAIL your-email@example.com
ENV PGADMIN_DEFAULT_PASSWORD yourSecurePassword

# Expose the pgAdmin port
EXPOSE 80
